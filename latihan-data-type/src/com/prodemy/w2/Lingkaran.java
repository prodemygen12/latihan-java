package com.prodemy.w2;

public class Lingkaran extends BidangDatar{
	
	private double diameter;
	
	public Lingkaran(double diameter) {
		super();
		this.diameter = diameter;
	}
	
	public Lingkaran() {
		super();
		// TODO Auto-generated constructor stub
	}

	public double getDiameter() {
		return diameter;
	}
	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}
	@Override
	public String toString() {
		return "Luas Lingkaran :";
	}
	@Override
	 public double getLuas() {
		 return Math.PI*Math.pow(this.diameter/2, 2);
	 }
}
