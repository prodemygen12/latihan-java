package com.prodemy.w2;

public class Segitiga extends BidangDatar{

	private double alas;
	private double tinggi;
	
	public Segitiga(double alas, double tinggi) {
		super();
		this.alas = alas;
		this.tinggi = tinggi;
	}

	public Segitiga() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public double getAlas() {
		return alas;
	}

	public void setAlas(double alas) {
		this.alas = alas;
	}

	public double getTinggi() {
		return tinggi;
	}

	public void setTinggi(double tinggi) {
		this.tinggi = tinggi;
	}

	public String toString() {
		return "Luas Segitiga :";
	}
	
	public double getLuas() {
		return (this.alas*this.tinggi)/2;
	}
}
