package com.prodemy.w2;

public class Persegi extends BidangDatar {
	
	private double sisi;
	
	public Persegi(double sisi) {
		super();
		this.sisi = sisi;
	}
	
	public Persegi() {
		super();
		// TODO Auto-generated constructor stub
	}

	public double getSisi() {
		return sisi;
	}

	public void setSisi(double sisi) {
		this.sisi = sisi;
	}
	
	public String toString() {
		return "Luas Persegi : ";
	}

	public double getLuas() {
		// TODO Auto-generated method stub
		return this.sisi*this.sisi;
	}
}
