package com.prodemy.w2;

public class CaseBidangDatar {
	
	public static void main(String[] args) {
		Segitiga bd = new Segitiga(10, 5);
		Lingkaran l = new Lingkaran(7);
		
		BidangDatar[] bidangDatars = new BidangDatar[2];
		bidangDatars[0] = bd;
		bidangDatars[1] = l;
		
		for(BidangDatar b: bidangDatars) {
			System.out.println(b +" "+ b.getLuas());
		}
	}
}
