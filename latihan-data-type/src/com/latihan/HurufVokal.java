package com.latihan;

public class HurufVokal {

	private char huruf;
	private int jumlah;
	
	public HurufVokal(char huruf, int jumlah) {
		super();
		this.huruf = huruf;
		this.jumlah = jumlah;
	}

	public char getHuruf() {
		return huruf;
	}

	public void setHuruf(char huruf) {
		this.huruf = huruf;
	}

	public int getJumlah() {
		return jumlah;
	}

	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}
	
	public void tambahkan() {
		this.jumlah++;
	}
}
