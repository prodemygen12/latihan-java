package com.latihan;

public class LatihanHurufVokal {

	static HurufVokal[] hasil = {
			new HurufVokal('a', 0),
			new HurufVokal('i', 0),
			new HurufVokal('u', 0),
			new HurufVokal('e', 0),
			new HurufVokal('o', 0)
	};
	
	public static void tambahkan(char c) {
		for(int i=0; i<hasil.length; i++) {
			HurufVokal tmp = hasil[i];
			
			if(tmp.getHuruf() == c) {
				tmp.tambahkan();
			}
		}
	}
	
	public static void tampilkanHasil() {
		for(int i=0; i<hasil.length; i++) {
			HurufVokal tmp = hasil[i];
			System.out.println("Jumlah huruf "+ tmp.getHuruf() + " : " + tmp.getJumlah());
		}
			
	}
	
	public static void main(String[] args) {
		
		String kalimat = "Pro Sigmaka Mandiri";
		
		for(int i=0; i< kalimat.length(); i++) {
			char charToCheck = kalimat.toLowerCase().charAt(i);
			
			if(Utility.isVowel(charToCheck)) {
				tambahkan(charToCheck);
			}
		}
		tampilkanHasil();
	}
}
