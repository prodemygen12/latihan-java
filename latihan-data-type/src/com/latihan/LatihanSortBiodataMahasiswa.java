package com.latihan;

import java.util.Scanner;

public class LatihanSortBiodataMahasiswa {

	public static void main(String[] args) {
		Mahasiswa[] bio = {
				new Mahasiswa("G01", "Deni", "Depok", "MTK", "FMIPA", new MyDate(25, 12, 1999)),
				new Mahasiswa("G04", "Adrisa", "Jakarta", "MTK", "FMIPA", new MyDate(21, 9, 1999)),
				new Mahasiswa("G08", "Devi", "Bandung", "MTK", "FMIPA", new MyDate(31, 5, 2000)),
				new Mahasiswa("G15", "Cici", "Jogja", "MTK", "FMIPA", new MyDate(1, 1, 2001))
		};
		
		for(int i=0; i<bio.length; i++) {
			for(int j=i+1; j<bio.length; j++) {
				if(bio[i].getNama().compareTo(bio[i].getNama())>0) {
					Mahasiswa temp = bio[i];
					bio[i]=bio[j];
					bio[j]= temp;
				}
			}
		}
		for(Mahasiswa b:bio) {
			System.out.println(b);
			System.out.println("-----------------");
		}
	}
}
