package com.latihan;

public class Utility {

	public static void cetakArray2D(String[][] args) {
		for (int baris = 0; baris < args.length; baris++) {
			for (int kolom = 0; kolom < args[baris].length; kolom++) {
				if (args[baris][kolom] != null) {
					System.out.print(args[baris][kolom] + " ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}

//	Huruf vokal true or false
	public static boolean isVowel(char c) {
		char[] daftarHurufVokal = { 'a', 'i', 'u', 'e', 'o' };

		for (int i = 0; i < daftarHurufVokal.length; i++) {
			char tmp = daftarHurufVokal[i];

			if (c == tmp) {
				return true;
			}
		}
		return false;
	}

	public static String addLeftPadding(int x, int len, String pad) {
		String result = "";
		int len1 = len;

		// proses membentuk karakter pad sebanyak len
		while (len > 0) {
			result += pad;
			len--;
		}

		result += String.valueOf(x);
		int lenStr = result.length();
		result = result.substring(lenStr - len1);

		return result;
	}
}
