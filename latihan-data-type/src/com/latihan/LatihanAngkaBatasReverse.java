package com.latihan;

import java.util.Scanner;

public class LatihanAngkaBatasReverse {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan Array Ganjil: ");
		int n = input.nextInt();
		
		int[] angkaBatas = new int[n];
		int nilai_tengah = (n+1)/2;
		
		for(int i = 0; i < nilai_tengah; i++) {
			angkaBatas[i] = i + 1;
		}
		for(int i = nilai_tengah; i < angkaBatas.length; i++) {
			angkaBatas[i] = angkaBatas[i-1] - 1;
		}
		
		for(int a:angkaBatas) {
			System.out.print(a + " ");
		}
	}
}
