package com.latihan;

import java.util.Arrays;

public class LatihanFibonacci {

	public static void main(String[] args) {
		
		int n = 11;
		int[] fib = new int[n];
		fib[0] = 1;
		fib[1] = 1;
		
		for(int i = 2; i < fib.length; i++) {
			fib[i] = fib[i-1] +fib[i-2];
		}
		
		for(int f:fib) {
			System.out.print(f + " ");
		}
	}
}
