package com.latihan;

import java.util.Scanner;

public class LatihanRekomendasiBarang {

	public static void main(String[] args) {
		Barang[] stok = { new Barang("Buavita", 7000), new Barang("Oreo", 6000), new Barang("Tango", 5000),
				new Barang("Aqua", 3000) };

		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan Jumlah Uang : ");
		int uang = input.nextInt();

		Barang rekomendasi = new Barang("", 0);

		for (Barang b : stok) {
			if (uang >= b.getHargaBarang()) {
				if(b.getHargaBarang()>rekomendasi.getHargaBarang()) {
					rekomendasi = b;
				}
			}
		}
		System.out.println(rekomendasi);
	}
}
