package com.latihan;

import java.util.Scanner;

public class LatihanCrossFibonacciReverse {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan jumlah baris/kolom (Ganjil) : ");
		int n = input.nextInt();

		String[][] cross = new String[n][n];
		int nilai_tengah = (n - 1) / 2;

		for (int baris = 0; baris < cross.length; baris++) {
			int nilai1 = 1;
			int nilai2 = 1;

			for(int kolom = 0; kolom < nilai_tengah; kolom++) {
				if (baris == kolom || baris + kolom == n - 1) {
					cross[baris][kolom] = String.valueOf(nilai1);
				}
				int fibonacci = nilai1 + nilai2;
				nilai1 = nilai2;
				nilai2 = fibonacci;
			}
			for(int kolom=nilai_tengah; kolom< cross.length; kolom++) {
				if(baris==kolom || baris+kolom == n-1) {
					cross[baris][kolom]= String.valueOf(nilai1);
				}
				int fibonacciReverse = nilai2 - nilai1;
				nilai2 = nilai1;
				nilai1 = fibonacciReverse;
			}
		}
		Utility.cetakArray2D(cross);
	}
}
