package com.latihan;

import java.util.Scanner;

public class LatihanBiodataMahasiswa {

	public static void main(String[] args) {
		Mahasiswa[] bio = {
				new Mahasiswa("G01", "Deni", "Depok", "MTK", "FMIPA", new MyDate(25, 12, 1999)),
				new Mahasiswa("G04", "Adrisa", "Jakarta", "MTK", "FMIPA", new MyDate(21, 9, 1999)),
				new Mahasiswa("G08", "Devi", "Bandung", "MTK", "FMIPA", new MyDate(31, 5, 2000)),
				new Mahasiswa("G15", "Cici", "Jogja", "MTK", "FMIPA", new MyDate(1, 1, 2001))
		};
		
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan NIM : ");
		String NIM = input.next();
		
		Mahasiswa biodata = new Mahasiswa("", "", "", "", "", new MyDate(0, 0, 0));
		
		for(Mahasiswa b:bio) {
			if(NIM.equals(b.getNim())) {
				biodata = b;
			}
		}
		System.out.print(biodata);
	}
}
