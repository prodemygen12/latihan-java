package com.latihan;

public class Barang {

	private String namaBarang;
	private int hargaBarang;

	public Barang(String namaBarang, int hargaBarang) {
		super();
		this.namaBarang = namaBarang;
		this.hargaBarang = hargaBarang;
	}

	public String getNamaBarang() {
		return namaBarang;
	}


	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}


	public int getHargaBarang() {
		return hargaBarang;
	}


	public void setHargaBarang(int hargaBarang) {
		this.hargaBarang = hargaBarang;
	}

	@Override
	public String toString() {
		return "Nama Barang : " + namaBarang + ", Harga : " + hargaBarang;
	}


}
