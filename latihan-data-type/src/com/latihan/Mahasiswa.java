package com.latihan;

public class Mahasiswa {

	private String nim;
	private String nama;
	private String alamat;
	private String prodi;
	private String fakultas;
	private MyDate tanggal_lahir;
	
	public Mahasiswa(String nim, String nama, String alamat, String prodi, String fakultas, MyDate tanggal_lahir) {
		super();
		this.nim = nim;
		this.nama = nama;
		this.alamat = alamat;
		this.prodi = prodi;
		this.fakultas = fakultas;
		this.tanggal_lahir = tanggal_lahir;
	}

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getProdi() {
		return prodi;
	}

	public void setProdi(String prodi) {
		this.prodi = prodi;
	}

	public String getFakultas() {
		return fakultas;
	}

	public void setFakultas(String fakultas) {
		this.fakultas = fakultas;
	}

	public MyDate getTanggal_lahir() {
		return tanggal_lahir;
	}

	public void setTanggal_lahir(MyDate tanggal_lahir) {
		this.tanggal_lahir = tanggal_lahir;
	}
	
	@Override
	public String toString() {
		return "Data Mahasiswa \nNIM : " + this.nim + "\nNama : " + this.nama + "\nAlamat : " + this.alamat + "\nProdi : " + this.prodi + "\nFakultas : " + this.fakultas + "\nTanggal Lahir : " + this.tanggal_lahir;
	}

}
