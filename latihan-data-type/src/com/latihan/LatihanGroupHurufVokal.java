package com.latihan;

import java.util.ArrayList;
import java.util.List;

public class LatihanGroupHurufVokal {

	public static void main(String[] args) {
		String kalimat = "Pro Sigmaka Mandiri";
		String text = kalimat.toLowerCase();
		
		List<Character> result = new ArrayList<>();
// cek apakah huruf vokal		
		for(int i=0; i<text.length(); i++) {
			if(Utility.isVowel(text.charAt(i))) {
				
				// indexOf dalam Array list mengembalikan nilai -1 saat tidak ditemukan anggota
				if(result.indexOf(text.charAt(i))!= -1) {
					result.add(result.indexOf(text.charAt(i))+1, text.charAt(i));
				} else {
					result.add(text.charAt(i));
				}
			}
		}
		System.out.print(result);
	}
}
