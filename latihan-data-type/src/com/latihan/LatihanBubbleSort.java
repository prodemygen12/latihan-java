package com.latihan;

public class LatihanBubbleSort {

	public static void main(String[] args) {
		int arr[] = { 21, 3, 4, 16, 1, 5, 7, 40 };

		for (int i = 0; i < arr.length; i++) {
			for (int j = 1; j <= arr.length - 1; j++) {
				if(arr[j-1]>arr[j]) {
					int tmp = arr[j-1];
					arr[j-1]=arr[j];
					arr[j]= tmp;
				}
			}
		}
		for(int i=0; i<arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}
}
