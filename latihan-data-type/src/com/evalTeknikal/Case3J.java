package com.evalTeknikal;

import java.util.Scanner;

public class Case3J {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan jumlah baris (n) : ");
		int n = input.nextInt();
		System.out.println("Masukkan jumlah kolom (m) : ");
		int m = input.nextInt();

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				if (i == 1 || j == m/2  || j == 1 || j == n) {
					System.out.print("*" + " ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println("");
		}

	}
}
