package com.evalTeknikal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Case1J {

	public static void main(String[] args) {
		/*
		 * int n = 8; 
		 * int t = 5;
		 * 
		 * 
		 * int[] width = {2, 3, 1, 2, 3, 2, 3, 3}; 
		 * int[][] cases = {{0, 3}, {4, 6}, {6,7}, {3, 5}, {0, 7}};
		 */

		int[] width = new int[] { 10, 7, 6, 5,10};
		int[][] cases = new int[][] { { 0, 3 }, { 0, 1 }, { 1, 2 }, { 0, 4 }, { 1, 3 } };

		List<Integer> resultMin = new ArrayList<>();

		for (int i = 0; i < cases.length; i++) {
			List<Integer> w = new ArrayList<>();

			for (int j = cases[i][0]; j <= cases[i][1]; j++) {
				w.add(width[j]);
			}
			resultMin.add(Collections.min(w));
		}

		for (int i = 0; i < resultMin.size(); i++) {
			System.out.println(resultMin.get(i));

		}
	}
}
