package com.evalTeknikal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Case1JJ {

	static List<ArrayList<Integer>> create2DArrayList() {
		
		ArrayList<ArrayList<Integer>> cases = new ArrayList<ArrayList<Integer>>();
		
		cases.add(new ArrayList<Integer>());
		cases.add(0, new ArrayList<>(Arrays.asList(0, 3)));		
		cases.add(1, new ArrayList<>(Arrays.asList(4, 6)));	
		cases.add(2, new ArrayList<>(Arrays.asList(6, 7)));
		cases.add(3, new ArrayList<>(Arrays.asList(3, 5)));
		cases.add(4, new ArrayList<>(Arrays.asList(0, 7)));
		return cases;		
	}
	
	public static void main(String[] args) {
		
		List<Integer> width = new ArrayList<>();
		width.add(2);
		width.add(3);
		width.add(1);
		width.add(2);
		width.add(3);
		width.add(2);
		width.add(3);
		width.add(3);
		
		List<Integer> resultMin = new ArrayList<>();
		
			for (int i = 0; i < create2DArrayList().size(); i++) {
				List<Integer> w = new ArrayList<>();
	
				for (int j = create2DArrayList().get(i).get(0) ; j <= create2DArrayList().get(i).get(0);j++) {
					w.add(width.get(j));
				}
				resultMin.add(Collections.min(w));
			}
	
			for(int i=0; i<resultMin.size(); i++) {
				System.out.println(resultMin.get(i));
				
			}
//			System.out.println(create2DArrayList().get(0).get(1));
	}
}
