package com.evalTeknikal;

public class Case2J {

	public static void main(String[] args) {
		int n = 32;
		int c = 4;
		int m = 3;
		
		int totalCoklat = n/c;
		int kembalian = n%c;
		int wrappers = totalCoklat;
		
		while(wrappers>=m || wrappers==m) {
			int freeCoklat = wrappers/m;
			totalCoklat += freeCoklat;
			wrappers = freeCoklat + (wrappers % m);
		}
		System.out.println(totalCoklat);
		System.out.println("Sisa Uang : " + kembalian);
	}
}
